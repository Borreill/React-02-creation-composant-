import React from 'react';

import './App.css';
import Contact from './components/Contact';

function App() {
  return (
    <div className="App">

      <Contact
        online avatar="https://cdn140.picsart.com/266428059017202.jpg?c256x256"
        name="Demorgogeon"
      />

      <Contact
        offline avatar="https://www.stylist.co.uk/images/app/uploads/2019/09/30174052/stranger_things_s03e03_44m5s63420f_r-crop-1569861674-1018x1018.jpg?w=256&h=256&fit=max&auto=format%2Ccompress"
        name="Eleven"
      />

      <Contact
        online avatar="https://i.pinimg.com/474x/4a/17/39/4a1739f2ce0cd5f467ade37f4ebb95e6.jpg"
        name="Jim Hooper"
      />

    </div>
  );
}

export default App;